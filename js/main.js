import {
  anabled,
  deletePreventDefault,
  disableInput,
  getInformation,
  preventDefault,
  render,
  resetForm,
  showInformation,
} from "./controller/controller.js";
import {
  checkEmpty,
  checkLinkImg,
  checkNumber,
  checkText,
  duplicateCheck,
} from "./controller/validate.js";
import { batLoading, Product, tatLoading } from "./model/model.js";
import {
  dataService,
  deleteDataService,
  postDataService,
  repairDataService,
  updateDataService,
} from "./service/service.js";

// gọi lần đầu khi load trang
let fetchAPI = () => {
  batLoading();
  dataService()
    .then((res) => {
      let data = res.data;
      // convert dữ liệu từ backend
      let listDesc = data.map((item) => {
        return new Product(
          item.id,
          item.name,
          item.price,
          item.img,
          item.screen,
          item.frontCamera,
          item.backCamera,
          item.type,
          item.desc
        );
      });
      console.log(listDesc);
      render(listDesc);
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
};
fetchAPI();
// thêm Product
document.getElementById("btn-submit").addEventListener("click", () => {
  let product = getInformation();
  batLoading();

  let isValid = true;
  isValid = isValid & checkEmpty(product.id, "tbID");
  isValid =
    isValid & checkEmpty(product.name, "tbTen") &&
    checkText(product.name, "tbTen");
  isValid =
    isValid & checkEmpty(product.price, "tbGia") &&
    checkNumber(product.price, "tbGia");
  isValid =
    isValid & checkEmpty(product.img, "tbHinh") &&
    checkLinkImg(product.img, "tbHinh");
  isValid = isValid & checkEmpty(product.screen, "tbScreen");
  isValid = isValid & checkEmpty(product.frontCamera, "tbFrontCamera");
  isValid = isValid & checkEmpty(product.backCamera, "tbBackCamera");
  isValid = isValid & checkEmpty(product.type, "tbType");
  isValid =
    isValid & checkEmpty(product.desc, "tbMoTa") &&
    checkText(product.desc, "tbMoTa");
  preventDefault();
  if (!isValid) {
    return;
  }

  postDataService(product)
    .then(function (res) {
      fetchAPI();
      Swal.fire("Thêm sản phẩm mới thành công");
      deletePreventDefault();
      resetForm();
      tatLoading();
    })
    .catch(function (err) {
      Swal.fire("Thêm sản phẩm mới thất bại");
      tatLoading();
    });
});
// Xóa Product

let deleteProduct = (id) => {
  batLoading();
  deleteDataService(id)
    .then((res) => {
      fetchAPI();
      Swal.fire("Xóa thành công");
      tatLoading();
    })
    .catch((err) => {
      Swal.fire("Xóa thất bại !");
      tatLoading();
    });
};
window.deleteProduct = deleteProduct;
// Sửa product
let repairProduct = (id) => {
  batLoading();
  repairDataService(id)
    .then(function (res) {
      fetchAPI();
      showInformation(res.data);
      disableInput();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
};
window.repairProduct = repairProduct;
// update product
let updateProduct = () => {
  batLoading();
  let getProduct = getInformation();
  updateDataService(getProduct)
    .then(function (res) {
      fetchAPI();
      Swal.fire("Cập nhật sản phẩm thành công");
      anabled();
      resetForm();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
      Swal.fire("Cập nhật sản phẩm thất bại");
      tatLoading();
    });
};
window.updateProduct = updateProduct;

document.getElementById("search").addEventListener("click", () => {
  const search = document.getElementById("inputTK").value;
  dataService().then((res) => {
    let newProduct = res.data.filter((item) => {
      return item.type.toUpperCase().includes(search.toUpperCase());
    });
    render(newProduct);
  });
});
