import { Product } from "../model/model.js";

export let getInformation = () => {
  let id = document.getElementById("STT").value.trim();
  let name = document.getElementById("TenSP").value.trim();
  let price = document.getElementById("GiaSP").value.trim();
  let avatar = document.getElementById("HinhSP").value.trim();
  let screen = document.getElementById("screen").value.trim();
  let frontCamera = document.getElementById("front-camera").value.trim();
  let backCamera = document.getElementById("back-camera").value;
  let type = document.getElementById("type").value.trim();
  let description = document.getElementById("MoTa").value.trim();

  let product = new Product(
    id,
    name,
    price,
    avatar,
    screen,
    frontCamera,
    backCamera,
    type,
    description
  );
  return product;
};
export let showInformation = (product) => {
  document.getElementById("STT").value = product.id;
  document.getElementById("TenSP").value = product.name;
  document.getElementById("GiaSP").value = product.price;
  document.getElementById("HinhSP").value = product.img;
  document.getElementById("screen").value = product.screen;
  document.getElementById("front-camera").value = product.frontCamera;
  document.getElementById("back-camera").value = product.backCamera;
  document.getElementById("type").value = product.type;
  document.getElementById("MoTa").value = product.desc;
};

export let resetForm = () => {
  document.getElementById("myForm").reset();
};

export let disableInput = () => {
  document.getElementById("STT").disabled = true;
};
export let anabled = () => {
  document.getElementById("STT").disabled = false;
};

// render dữ liệu
export let render = (list) => {
  let content = "";
  list.forEach(function (goods) {
    content += `<tr>
        <td>${goods.id}</td>
        <td>${goods.name}</td>
        <td>${goods.price}</td>
        <td><img src="${goods.img}" style="width: 200px"alt="" /></td>
        <td>${goods.screen}</td>
        <td>${goods.frontCamera}</td>
        <td>${goods.backCamera}</td>
        <td>${goods.type}</td>
        <td>${goods.desc}</td> 
        <td>
        <button onclick="deleteProduct('${goods.id}')" class="btn btn-danger">Xóa</button>
        <button data-toggle="modal"
        data-target="#myModal" onclick="repairProduct(${goods.id})" class="btn btn-primary">Sửa</button>
        </td> 
        </tr>`;
  });
  // type : true = iphone , false =samsung
  document.getElementById("tblDanhSachSP").innerHTML = content;
};
// ngăn chặn hành vi mặc định
export let preventDefault = () => {
  let btn = document.getElementById("btn-submit");
  btn.setAttribute("data-toggle", "");
  btn.setAttribute("data-target", "");
};
export let deletePreventDefault = () => {
  let btn = document.getElementById("btn-submit");
  btn.setAttribute("data-toggle", "modal");
  btn.setAttribute("data-target", "#myModal");
};
